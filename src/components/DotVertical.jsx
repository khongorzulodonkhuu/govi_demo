import React from "react";

function DotVertical(props){
    return (
        <div class="uk-position-center-left  dotVertical uk-margin-medium-left">
        <ul class="uk-dotnav uk-dotnav-vertical">
            <li uk-slideshow-item="0" class="uk-active"><a href="#">Item 1</a></li>
            <li uk-slideshow-item="1"><a href="#">Item 2</a></li>
            <li uk-slideshow-item="2"><a href="#">Item 3</a></li>
        </ul>
    </div>
    );
}

export default DotVertical;