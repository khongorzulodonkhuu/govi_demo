import React from "react";

import images from "../data";
import Shirts from "./Shirts";
import {womenShirts} from "../data";
import WomenShirt from "./WomeShirs";

function SectionWomen(props) {
    var title = "女套衫";
    var footerText = "查看所有产品";
    return (
        <div class="sectionWomen">
            <div class="uk-container uk-container-expand">
                <h5 class="uk-margin-small-bottom">{title}</h5>
                <div class="uk-child-width-1-4@m uk-child-width-1-4@l uk-child-width-1-1@s uk-child-width-1-1  uk-grid-small uk-text-center" uk-grid="">

                {womenShirts.map((item) => <WomenShirt key={item.key} img={item.imgURL} />)}
                </div>
                <div uk-grid="" class="uk-margin-small-top">
                    <h5 class="">{footerText}</h5>
                    <span uk-icon="icon:  arrow-right"></span>
                </div>
            </div>
        </div>


    );
    

}

export default SectionWomen;