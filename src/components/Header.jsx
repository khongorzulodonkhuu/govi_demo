import React from "react";

function Header() {
  return <header >
            <div class="header_top uk-visible@l" data-height="39" >
                <div class="uk-container uk-container-large header_top_top">
                    <div class="uk-child-width-1-1  header_01" uk-sticky="top: #container-1; bottom: #animation" uk-grid>
                        <div>
                            <div class="uk-child-width-auto uk-flex uk-flex-center" uk-grid>
                                <div>
                                    <div class="uk-inline">
                                        <label class="uk-text-middle">打击地区顺丰包邮</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
  </header>
}

export default Header;