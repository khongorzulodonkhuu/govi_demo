import React from "react";


function SlideImage(props){
return (
    <div>
        <div class="uk-visible@xl">
             <li><img src={props.img}  class="sliderImageLarge"/></li>
        </div>
        <div class="uk-hidden@xl">
             <li><img src={props.img}  class="sliderImage"/></li>
        </div>
    </div>
);

}

export default SlideImage;