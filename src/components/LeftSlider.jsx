import React from "react";
import {sliderLeftImages } from "../data";


function Image(props){
    return  <li>
                 <img src={props.img} class="slideNavImage" />
            </li>
    }


function LeftSlider(props) {
    
return (
    <ul class="uk-slideshow-items itemsRight">
                    {
                        sliderLeftImages.map((item) => <Image img={item.imgURL}/>)
                    }
                </ul>
);
}

export default LeftSlider;