import React from "react";
import Shirts from "./Shirts";
import {images} from "../data";

function SectionFour(props) {
    var title = "男套衫";
    var footerText = "查看所有产品";
    return (
        <div class="sectionMan">
            <div class="uk-container uk-container-expand">
                <h5 class="uk-margin-small-bottom">{title}</h5>
                <div class="uk-child-width-1-4@m uk-child-width-1-4@l uk-child-width-1-1@s uk-child-width-1-1  uk-grid-small uk-text-center" uk-grid="">
                {images.map((item) => <Shirts key={item.key} img={item.imgURL} />)}
                </div>
                <div uk-grid="" class="uk-margin-small-top">
                    <h5 class="">{footerText}</h5>
                    <span uk-icon="icon:  arrow-right"></span>
                </div>
            </div>
        </div>


    );
    

}

export default SectionFour;