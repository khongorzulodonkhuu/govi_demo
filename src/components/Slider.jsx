import React from "react";
import DotVertical from "./DotVertical";
import SlideItem from "./SlideItem";
import LeftSlider from "./LeftSlider";
import SliderNav from "./SliderNav";
import SliderText from "./SliderText";

function Slider() {
var title ="初秋";
var verticalTitle = "关于戈壁羊绒"
return( 
<div class="slider-item uk-visible@m uk-visible@l" uk-grid=""> 
    <div class="uk-width-2-5@l uk-width-2-5@m  left-slider">
        <div class="uk-dark customDark" uk-slideshow="">
                <div class="uk-card">
                        <SlideItem/>
                        <DotVertical/>
                </div>
        </div>
    </div>
        <div class="uk-width-3-5@l uk-width-3-5@m">
            <div class=" uk-dark"  uk-slideshow="">
                <LeftSlider />
                <SliderNav/>
            </div>
            <SliderText title={title} verticalTitle={verticalTitle}/>
    </div>
</div>
);
}

export default Slider;