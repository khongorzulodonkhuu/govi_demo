import React from "react";

import images from "../data";
import Shirts from "./Shirts";
import {mixedImages} from "../data";
import WomenShirt from "./WomeShirs";
import CenterClipImage from "./CenterClipImage";
import SkirtItem from "./SkirtItem";

function SectionThird(props) {
    var title = "裙装";
    var footerText = "查看所有产品";
    return (
        <div class="sectionFour uk-hidden@xl">
            <div class="uk-container uk-container-expand">
                <div class="uk-child-width-1-3@m uk-child-width-1-3@l uk-child-width-1-1@s uk-child-width-1-1  uk-grid-small " uk-grid="">
                {mixedImages.map((item) => <SkirtItem key={item.key} title={item.name} img={item.imgURL} />)}
                </div>
            </div>
        </div>


    );
    

}

export default SectionThird;