import React from "react";
import FooterTitle from "./FooterTitle";

function Footer(props) {

  return <footer class="uk-visible@l uk-visible@m">
    <div class="uk-container uk-container-small  ">
    <div class="uk-child-width-expand@m uk-child-width-expand@l uk-child-width-1-1@s uk-child-width-1-1 uk-margin-medium-top " uk-grid="">
   <FooterTitle title="关于戈壁羊绒" text="戈壁羊绒历史"  subtext="新闻"/>
   <FooterTitle title="隐私政策" text="隐私政策" subtext="隐私政策" />
   <FooterTitle title="关注我家" text="Facebook" subtext="Instagram"  isIcon="true"/>
   <FooterTitle title="客户关怀" text="联系我们" subtext="羊绒护理"/>
</div>
    </div>
  </footer>
}

export default Footer;