import React from "react";

function Progress() {

    return(
        <progress id="js-progressbar" class="uk-progress progressBar" value="10" max="100"></progress>
    );

}

export default Progress;