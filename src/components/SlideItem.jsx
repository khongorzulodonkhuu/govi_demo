import React from "react";
import SlideImage from "./SlideImage";
import {sliderImages} from "../data";


function SlideItem(props) {
    return(
        <div  class=" uk-margin-small-left">
        <ul class="uk-slideshow-items  items">
            {
                sliderImages.map((item) => <SlideImage img={item.imgURL}/> )
            }
        
        </ul>
        
   </div>
    );

}

export default SlideItem;