import React from "react";
import Image from "./Avatar";
function CenterClipImage(props) {
    var title ="dress with drawstrings";
    
    var buttonTitle ="SHOP NOW";
    return (
        <div>
            <div class="uk-card  man-card uk-inline-clip uk-transition-toggle">
                <Image img={props.img} />
                <div class="uk-transition-fade uk-position-cover uk-position-expand uk-overlay uk-overlay-primary uk-flex uk-flex-center uk-flex-middle">
                    <div>
                    <p class="uk-h4 uk-margin-remove-top centerClipTitle">{title}</p>
                    <p class="uk-h4 uk-margin-small-top  centerClipButtonText">{buttonTitle}
                    <p class="dividerCustom"/></p>
                    
                </div>
            </div>
            </div>
    </div>
    );

}

export default CenterClipImage;