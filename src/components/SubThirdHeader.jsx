import React from "react";


function SubThirdHeader() {
    return <div className="subHeader uk-visible@m uk-visible@l">
            <div class="header-nav">
            <div class="uk-navbar-container  header-nav-top " uk-sticky="">
            <nav class="uk-navbar top-nav" uk-navbar>
                        <div class="uk-navbar-center">
                        <ul class="uk-navbar-nav">
                                   <li><a>新品</a></li>
                                   <li><a>女士专区</a></li>
                                   <li><a>男士专区</a></li>
                                   <li><a>家纺专区</a></li>
                                   <li><a>配饰专区</a></li>
                                   <li><a>童装专区</a></li>
                                   <li><a> 戈壁日记</a></li>
                                </ul>
                        </div>
                </nav>
                </div>
            </div>
    </div>
}

export default SubThirdHeader;