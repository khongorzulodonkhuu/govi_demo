import React from "react";
import Shirts from "./Shirts";
import {images} from "../data";

function SectionFive(props) {
    var title = "男套衫";
    var footerText = "查看所有产品";
    return (
            <div class="uk-container uk-container-expand uk-visible@m uk-visible@l uk-margin-medium-top">
            <div class="uk-flex-middle uk-flex uk-text-center" uk-grid="">
            <div class="uk-width-1-3@m">
        <div class="uk-card ">
        <div class="customCard">
                    <img src="/images/mixed/womenOne.jpeg"/>
                </div>
        </div>
    </div>
    <div class="uk-width-auto@m uk-visible@l">
        <div class="uk-card ">
        <div class=" uk-align-center " style={{fontSize: 80, width:220, color:"#000"}}>
                   <ul class="uk-list">
                        <li>初秋
                        <hr/>焕新</li>
                   </ul>
                </div>
        </div>
    </div>
   
    <div class="uk-width-expand">
        <div class="uk-card  bottom-image " >
        <img  src="/images/mixed/womenTwo.jpeg" />
        </div>
    </div>
</div>
        </div>


    );
    

}

export default SectionFive;