import React from 'react';
import SlideImage from './SlideImage';

function SliderNav() {
    return (
        <div>
            {/* <div class="uk-visible@s">
            <a class="uk-slidenav-medium slideNavLeftSmall uk-position-center-right uk-position-medium uk-hidden-hover" uk-slidenav-next="" uk-slideshow-item="next"></a>
                <a class="uk-slidenav-medium slideNavRightSmall uk-position-bottom-right uk-position-medium uk-hidden-hover "  uk-slidenav-previous="" uk-slideshow-item="previous"></a>
            </div> */}
            <div class="uk-hidden@xl">
                <a class="uk-slidenav-medium slideNavLeft uk-position-center-right uk-position-medium uk-hidden-hover" uk-slidenav-next="" uk-slideshow-item="next"></a>
                <a class="uk-slidenav-medium slideNavRight uk-position-bottom-right uk-position-medium uk-hidden-hover "  uk-slidenav-previous="" uk-slideshow-item="previous"></a>
            </div>
            <div class="uk-visible@xl">
                <a class="uk-slidenav-medium slideNavLeftLarge uk-position-center-right uk-position-medium uk-hidden-hover" uk-slidenav-next="" uk-slideshow-item="next"></a>
                <a class="uk-slidenav-medium slideNavRightLarge uk-position-bottom-right uk-position-medium uk-hidden-hover "  uk-slidenav-previous="" uk-slideshow-item="previous"></a>
            </div>
        </div>
    );
}
export default SliderNav;