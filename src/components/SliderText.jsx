import React from 'react';

function SliderText(props) {
    return (
       
        <div >
            

        <div class="uk-visible@xl">
        <div class="slideCoverTextLarge">
            <p>{props.title}</p>
            <hr class="uk-divider-custom"></hr>
        </div>
            <div class="slideVerticalTextLarge">
                <p>{props.verticalTitle}</p>
            </div>
        </div>
        <div class="uk-hidden@xl">
        <div class="slideCoverText">
            <p>{props.title}</p>
            <hr class="uk-divider-custom"></hr>
        </div>
            <div class="slideVerticalText">
                <p>{props.verticalTitle}</p>
            </div>
        </div>
        </div>
    );

}

export default SliderText;