import React from "react";

import images from "../data";
import Shirts from "./Shirts";
import {womenPullovers} from "../data";
import WomenShirt from "./WomeShirs";
import CenterClipImage from "./CenterClipImage";
import Progress from "./Progress";

function SectionLast(props) {
    var title = "精纺系列";
    
    return (
        <div class="sectionFour">
            <div class="uk-container uk-container-expand">
                <h5 class="uk-margin-small-bottom">{title}</h5>
                <div class="uk-grid-collapse uk-child-width-1-4@m uk-child-width-1-4@l uk-child-width-1-1@s uk-child-width-1-1 uk-text-center " uk-grid="">
                {womenPullovers.map((item) => <CenterClipImage key={item.key} img={item.imgURL} />)} 
            </div>
            <Progress/>
            </div>
        </div>


    );
    

}

export default SectionLast;