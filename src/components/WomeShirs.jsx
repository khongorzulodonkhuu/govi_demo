import React from "react";
import Image from "./Avatar";
function WomenShirt(props) {
    var title ="BOAT NECK DRESS";
    var price = "$350";
    var color ="+6 Colors";
    return (
        <div>
            <div class="uk-card  man-card uk-inline-clip uk-transition-toggle">
                <Image img={props.img} />
                <div class="uk-transition-slide-bottom uk-position-bottom uk-overlay uk-overlay-default">
                <div class="uk-text-left womenShirt">
            <p class="uk-h6 uk-margin-remove">{title}</p>
            <p class="uk-h6 uk-margin-remove">{price}</p>
                <p class=" uk-margin-small-top uk-margin-remove-bottom ">{color} </p>
                    </div>
            </div>
            </div>
    </div>
    );

}

export default WomenShirt;