import React from "react";

function FooterTitle(props) {

    return(
        <div className="">
            <div class="uk-margin uk-text-left@l uk-text-left@m uk-text-center@s uk-text-center footerTitle">
                <p class="title">{props.title}</p>
                    <ul class="uk-list uk-list-collapse dot">
                        <li class="text uk-margin-remove-bottom">{ props.isIcon ?  <span class="uk-sortable-handle uk-margin-small-right" uk-icon="icon: facebook"></span> : ""}{props.text}</li>   
                        <li class="text uk-margin-small-top ">{ props.isIcon ?  <span class="uk-sortable-handle uk-margin-small-right" uk-icon="icon: instagram"></span> : ""}{props.subtext}</li>   
                    </ul>
            </div>

            <div>
        
    </div>
        </div>
    );
}

export default FooterTitle;