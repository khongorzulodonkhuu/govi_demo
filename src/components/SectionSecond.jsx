import React from "react";

import images from "../data";
import Shirts from "./Shirts";
import {coupleImages} from "../data";

import SecondItem from "./SecondItem";

function SectionSecond(props) {
    var title = "裙装";
    var footerText = "查看所有产品";
    return (
        <div class="sectionSecond uk-visible@m uk-visible@l">
            <div class="uk-container uk-container-expand">
                <div class="uk-child-width-1-2@m uk-child-width-1-2@l uk-child-width-1-1@s uk-child-width-1-1   uk-text-center  uk-grid-small" uk-grid="">
                {coupleImages.map((item) => <SecondItem key={item.key} title={item.name} img={item.imgURL} />)}
                </div>
            </div>
        </div>


    );
    

}

export default SectionSecond;