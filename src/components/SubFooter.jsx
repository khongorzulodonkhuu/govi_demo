import React from "react";

function SubFooter() {
  var footerText = "戈壁羊绒 Gobi Cashmere China";

  const currentYear = new Date().getFullYear();

  return <div className="subFooter">
  <div class="uk-container uk-container-xlarge">
      <hr></hr>
    </div>
    <p>
      &copy; {currentYear}  {footerText}
    </p>
  </div>
}

export default SubFooter;