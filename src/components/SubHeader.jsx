import React from "react";


function SubHeader() {
    return <div className="subHeader">
            <div class="header-nav">
            <div class="uk-navbar-container  header-nav-top " uk-sticky="">
            <nav class="uk-navbar top-nav" uk-navbar>
            <div class="uk-navbar-left">
                            <div class="uk-visible@l">
                                <ul class="uk-navbar-nav list-nav">
                                    <li><a href="">语言选择</a>
                                        <div class="uk-navbar-dropdown">
                                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                                <li><a href="">List</a></li>
                                                <li><a href="">List</a></li>
                                                <li><a href="">List</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="uk-float-left" uk-icon="icon: search; ratio: 1" >
                                        </a>
                                        <a href="">请输入搜索的关键词</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="uk-navbar-center">
                            <img src="images/logo.svg" class=""/>
                        </div>
                        <div class="uk-navbar-right">
                        <div class="uk-visible@m">
                                <ul class="uk-navbar-nav">
                                    <li>
                                    <a class="uk-float-right" uk-icon="icon: heart; ratio: 1" >
                                    </a>
                                    </li>
                                    <li>
                                    <a class="uk-float-right" uk-icon="icon: cart; ratio: 1" >
                                    </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="uk-hidden@m" >
                                <a class="uk-float-right" uk-icon="icon: list; ratio: 1.5" uk-toggle="target: #offcanvas-overlay">
                                    </a>
                            </div>
                        </div>
                        
                </nav>
                </div>
            </div>
    </div>
}

export default SubHeader;