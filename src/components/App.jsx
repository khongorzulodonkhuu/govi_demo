import React from "react";
import Header from "./Header";
import Footer from "./Footer"
import SubHeader from "./SubHeader";
import SubThirdHeader from "./SubThirdHeader";
import Slider from "./Slider";
import SubFooter from "./SubFooter";
import SectionFour from "./SectionFour";
import SectionWomen from "./SectionWomen";
import SectionLast from "./SectionLast";
import SectionThird from "./SectionThird";
import SectionSecond from "./SectionSecond";
import SectionFive from "./SectionFive";
import SectionText from "./SectionText";


function App() {
  return <div>
    <Header/> 
    <SubHeader/>
    <SubThirdHeader/>
    <Slider />
    <SectionText/>
    <SectionThird/>
    <SectionSecond/>
    <SectionWomen/>
    <SectionFour/>
    <SectionFive/>
    <SectionLast/>
    <Footer/>
    <SubFooter/>
  </div>
}

export default App;
