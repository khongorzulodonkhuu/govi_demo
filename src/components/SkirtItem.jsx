import React from "react";
import Image from "./Avatar";
import MixedImageItem from "./MixedImageItem";
function SkirtItem(props) {
   
    return (
        <div>
            <div class="uk-card  mix-card uk-inline-clip uk-transition-toggle">
                <MixedImageItem img={props.img} />
                <div class="uk-transition-fade uk-position-cover uk-position-expand uk-overlay uk-overlay-primary uk-flex uk-flex-center uk-flex-middle">
                    <div>
                    <p class="dividerCustomTop uk-margin-remove-bottom"/>
                    <p class="uk-h4 uk-margin-small-top" >{props.title}</p>
                </div>
            </div>
            <div class="uk-position-bottom-center uk-margin-small-bottom">
            <div>
                    <p class="dividerCustomTop uk-margin-remove-bottom"/>
                    <p class="uk-h4 uk-margin-small-top white">{props.title}</p>
                </div>
            </div>
            </div>
    </div>
    );

}

export default SkirtItem;