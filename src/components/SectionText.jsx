import React from 'react';

function SectionText() {
    var text = "春意弥漫空中！是的，现在您就可以穿着全新羊绒服上街了！具有轻盈柔和质感的#全新春夏系列，将在这个不一样的季节为您带来春夏独特的温暖和崭新体验！";
    return (
    <div class="uk-container uk-container-small uk-margin-remove-top  sectionText uk-visible@m uk-visible@l">
        <div class="uk-text-center ">
            <p>{text}</p>
        </div>
        
    </div>
    );
}

export default SectionText;