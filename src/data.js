 const womenShirts = [
    {
      id:1,
      imgURL:
      "../../images/womenShirts/1.png",
    },
    {
      id:2,
      imgURL:
      "../../images/womenShirts/2.png",
    },
    {
      id:3,
      imgURL:
      "../../images/womenShirts/3.png",
    },
    {
      id:4,
      imgURL:
      "../../images/womenShirts/4.png",
    },
   
  ];

  const images = [
    {
        id:1,
        imgURL:
          "../../images/manShirts/1.png",
    },
    {
      id:2,
      imgURL:
      "../../images/manShirts/2.png",
  },
  {
    id:3,
    imgURL:
    "../../images/manShirts/3.png",
},
{
  id:4,
  imgURL:
  "../../images/manShirts/4.png",
}
  ];

  const womenPullovers = [
    {
        id:1,
        imgURL:
          "../../images/womenPullover/1.png",
    },
    {
      id:2,
      imgURL:
          "../../images/womenPullover/2.png",
  },
  {
    id:3,
    imgURL:
    "../../images/womenPullover/3.png",
},
{
  id:4,
  imgURL:
  "../../images/womenPullover/4.png",
}
  ];


  const mixedImages = [
    {
        id:1,
        name:"裙装",
        imgURL:
          "../../images/mixed/1.jpeg",
    },
    {
      id:2,
      name:"套装",
      imgURL:
          "../../images/mixed/2.jpeg",
  },
  {
    id:3,
    name:"套衫",
    imgURL:
          "../../images/mixed/3.jpeg",
},

  ];
  const coupleImages = [
    {
        id:1,
        name:"裙装",
        imgURL:
          "../../images/mixed/sec1.jpeg",
    },
    {
      id:2,
      name:"套装",
      imgURL:
          "../../images/mixed/sec2.jpeg",
  }
  ];

  const sliderImages = [
    {
        id:1,
        name:"裙装",
        imgURL:
          "../../images/slider/1.png",
    },
    {
      id:2,
      name:"套装",
      imgURL:
          "../../images/slider/2.png",
  },
  {
    id:3,
    name:"裙装",
    imgURL:
      "../../images/slider/1.png",
},
  ];
  const sliderLeftImages = [
    {
        id:1,
        name:"裙装",
        imgURL:
          "../../images/slider/3.png",
    },
    {
      id:2,
      name:"套装",
      imgURL:
          "../../images/slider/4.jpeg",
  },
  
  ];

  export {images, womenShirts, womenPullovers, mixedImages, coupleImages, sliderImages, sliderLeftImages};

